export const blockchainRID = "DB37BFB5444AB83E1CA2C1A6C8A6C95F12B6CDC787CDC9B7EC6BBD9469EC4D5B";
//export const blockchainUrl = "http://api.nilsfohlin.com/"; // for fun on my node at home, also good for demo purpose 
export const blockchainUrl = "http://localhost:7743/"; // This is default value in node-config.properties file
export const vaultUrl = "https://dev.vault.chromia-development.com"; // Vault's url for SSO
