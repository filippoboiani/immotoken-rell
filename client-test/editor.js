import pcl from 'postchain-client';

import {
    SingleSignatureAuthDescriptor,
    FlagsType,
    User
  } from 'ft3-lib';

const ediPRIV = Buffer.from(
    '680cc3536bafa811949416807cc356189858b4c05f69832ed2e353fd02fbacc2',
    'hex'
);

const ediPUB = Buffer.from(
    '03c3557c33f2b7d69a72f72893146d2b278b1b9ec53b258ffae5b4346caa5b09d3',
    'hex'
);

const editor_keyPair = pcl.util.makeKeyPair();
editor_keyPair.pubKey = ediPUB;
editor_keyPair.privKey = ediPRIV;

const authDescriptor = new SingleSignatureAuthDescriptor(editor_keyPair.pubKey,[FlagsType.Transfer]);
export const editor = new User(editor_keyPair, authDescriptor);
