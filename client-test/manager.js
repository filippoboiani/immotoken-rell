import pcl from 'postchain-client';

import {
    SingleSignatureAuthDescriptor,
    FlagsType,
    User
  } from 'ft3-lib';

const manPRIV = Buffer.from(
    'b60ec1b51d4e28d81297fa8ac5a80df007709750ccd04a5098188422add9c779',
    'hex'
);

const manPUB = Buffer.from(
    '033eec59d704e88c7ea86a0a4dece8d455d712bcddb4b9f6e64b9fdec9f71f3ab8',
    'hex'
);

const manager_keyPair = pcl.util.makeKeyPair();
manager_keyPair.pubKey = manPUB;
manager_keyPair.privKey = manPRIV;

const authDescriptor = new SingleSignatureAuthDescriptor(manager_keyPair.pubKey,[FlagsType.Account, FlagsType.Transfer]);
export const manager = new User(manager_keyPair, authDescriptor);





