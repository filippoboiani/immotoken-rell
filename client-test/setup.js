import { blockchainRID, blockchainUrl } from './config.js'; // Blockchain config
import { admin, adminSession } from './admin.js'; // Use these to act as root!
import { organization, orgSession } from './org.js'; // use these to act  as client!
import { manager, managerSession } from './manager.js'; // Use these to act as manager!
import { editor, editorSession } from './editor.js'; // Use these to act as manager!
import { superAdmin, superSession } from './superadmin.js'; // Use these to act as manager!
import { client, clientSession } from './cli.js'; // use these to act  as client!
import pcl from 'postchain-client';

import {op, Postchain, AssetBalance, SingleSignatureAuthDescriptor, FlagsType} from 'ft3-lib';

const chainId = Buffer.from(blockchainRID, 'hex');
const blockchain = await new Postchain(blockchainUrl).blockchain(chainId);

const rest = pcl.restClient.createRestClient(blockchainUrl, blockchainRID, 5)
const gtx = pcl.gtxClient.createClient(rest,Buffer.from(blockchainRID,'hex'),[]);

async function root_add_fund(_name, _user_auth){
    try{
        await adminSession.call(op('bloqhouse_real_estate.root_add_organization', _name, _user_auth));
    }catch(e){
        console.error(e);
    }
}

async function root_add_super_admin(_user_auth){
    try{
        await adminSession.call(op('bloqhouse_real_estate.root_add_super_admin', _user_auth));
    }catch(e){
        console.error(e);
    }
}

async function root_add_admin(_user_auth, _operable, _org){
    try{
        await adminSession.call(op('bloqhouse_real_estate.root_add_admin', _user_auth, _operable, _org));
    }catch(e){
        console.error(e);
    }
}

async function root_operable_user(_manAcc, _boolean){
    try{
        await adminSession.call(op('bloqhouse_real_estate.root_operable_user', _manAcc, _boolean));
    }catch(e){
        console.error(e);
    }
}

async function root_add_investor(_authDesc, _operable){
    try{
        await adminSession.call(op('bloqhouse_real_estate.root_add_investor', _authDesc, _operable));
    }catch(e){
        console.error(e);
    }
}

async function add_org_user(_admin_auth, _user_auth, _operable, _type){
    try{
        await managerSession.call(op('bloqhouse_real_estate.add_org_representative', _admin_auth, _user_auth, _operable, _type));
    }catch(e){
        console.error(e);
    }
}

async function get_user_account(_acc_id){
    try{
        const man = await gtx.query('bloqhouse_real_estate.get_user_by_id', {_id: _acc_id});
        return man;
    }catch(e){
        console.error(e);
    }
}


async function add_property(_name, _chainId, _nrOfFractions, _manAuth, _initValue, _currency, _start, _end, _extra){
    try{
        await managerSession.call(op('bloqhouse_real_estate.add_financial_instrument', _name, _chainId, _nrOfFractions, _manAuth, _initValue, _currency, _start, _end, _extra));   
    }catch(e){
        console.error(e);
    }
}

async function change_property_operable(_assetId, _boolean){
    try{
        await adminSession.call(op('bloqhouse_real_estate.root_operable_asset', _assetId, _boolean));   
    }catch(e){
        console.error(e);
    }
}

async function get_property_by_name(_name){
    try{
        const asset = await gtx.query('bloqhouse_real_estate.get_instrument_by_name', {_name: _name});
        return asset;
    }catch(e){
        console.error(e);
    }
}

async function transfer_to_investor_e(_manAuthId, _assetId, _investor, _nrOfShares){
    try{
        await editorSession.call(op('bloqhouse_real_estate.initial_transfer_instrument', _manAuthId, _assetId, _investor, _nrOfShares, []));   
    }catch(e){
        console.error(e);
    }
}

async function transfer_to_investor_m(_manAuthId, _assetId, _investor, _nrOfShares){
    try{
        await managerSession.call(op('bloqhouse_real_estate.initial_transfer_instrument',_manAuthId, _assetId, _investor, _nrOfShares, []));   
    }catch(e){
        console.error(e);
    }
}

// const fundname = "bloq";

// const assetName = "Gothenburg Castle";
// const numberOfShares = 10000;
// const initValue = 30000000;
// const currency = "EUR";
// const extra = '{"name":"John", "age":30, "car":null}'

// const assetName1 = "Stockholm Cabin";
// const numberOfShares1 = 5000;

// const start = Date.now();

// console.log("--------- ROOT INITIAL ---------")
// await root_add_fund(fundname, organization.authDescriptor);
// // await root_add_super_admin(superAdmin.authDescriptor);
// await root_add_admin(manager.authDescriptor, false, fundname);
// await root_add_investor(client.authDescriptor, true);

// console.log("--------- GET ORGANIZATION ---------")
// await gtx.query('bloqhouse_real_estate.get_org_by_id', {_id: organization.authDescriptor.id.toString('hex')});

// console.log("--------- GET USER ---------")
// const managerAccount = await get_user_account(manager.authDescriptor.id.toString('hex'));
// const clientAccount = await get_user_account(client.authDescriptor.id.toString('hex'));

// console.log("--------- CHANGE OPERABLE USERS ---------")
// await root_operable_user(managerAccount.account_id, true);
// // await change_user_operable(clientAccount.account_id, true);

// console.log("--------- ADD WITH MANAGER ---------")
// // const testauth = new SingleSignatureAuthDescriptor(edi_key.pubKey, [FlagsType.Transfer]);
// await add_org_user(manager.authDescriptor.id, editor.authDescriptor, true, "Editor");
// const editorAccount = await get_user_account(editor.authDescriptor.id.toString('hex'));

// console.log("--------- ADD PROPERTY ---------")
// await add_property(assetName, chainId, numberOfShares, manager.authDescriptor.id, initValue, currency, start, (start + 9000000));
// // await add_property(assetName1, chainId, numberOfShares1, managerAccount.account_id, manager.authDescriptor.id, initValue, currency, start, start);

console.log("--------- GET POROPERTY ---------")
const asset = await get_property_by_name("hej");


// console.log("--------- CHANGE OPERABEL ASSET ---------")
// // // //const asset1 = await get_property_by_name(assetName1);
// // await AssetBalance.getByAccountAndAssetId(managerAccount.account_id, asset.tokenId, blockchain);
// // await AssetBalance.getByAccountAndAssetId(clientAccount.account_id, asset.tokenId, blockchain);
// await change_property_operable(asset.tokenId, true);

// console.log("--------- INITIAL TRANSFER ----- KOLLA SESSION ---------")
// await transfer_to_investor_e(editor.authDescriptor.id, asset.tokenId, clientAccount.account_id, 100 );

// console.log("--------- GET INFO ---------")
// // //await transfer_to_investor(managerAccount.account_id, manager.authDescriptor.id, asset1.tokenId, clientAccount.account_id, 500 );
// // await AssetBalance.getByAccountAndAssetId(managerAccount.account_id, asset.tokenId, blockchain);
// // await AssetBalance.getByAccountAndAssetId(clientAccount.account_id, asset.tokenId, blockchain);
// // console.log("--------- HERE ---------")
// await gtx.query('bloqhouse_real_estate.get_all_owners_of_asset', {_asset_id: asset.tokenId});
// await gtx.query('bloqhouse_real_estate.get_all_assets_of_client', {_client_id: clientAccount.account_id});

// console.log("--------- TEST ---------")
// //await transfer_to_investor(editorAccount.account_id, testauth.id, asset.tokenId, clientAccount.account_id, 300 );
// await transfer_to_investor_m(manager.authDescriptor.id, asset.tokenId, clientAccount.account_id, 300 );
// await gtx.query('bloqhouse_real_estate.get_all_owners_of_asset', {_asset_id: asset.tokenId});
// await gtx.query('bloqhouse_real_estate.get_all_assets_of_client', {_client_id: clientAccount.account_id});