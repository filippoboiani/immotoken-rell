import pcl from 'postchain-client';

import {
    SingleSignatureAuthDescriptor,
    FlagsType,
    User,
  } from 'ft3-lib';

const orgPRIV = Buffer.from(
    'b32b29f13549b96574876dec0b241a589e05bb3c9968fa772178c947497b9e0b',
    'hex'
);

const orgPUB = Buffer.from(
    '029ceb9f1a6c4119ed1016b68d23be045ad5cfb93f92dead12fe2a0c46cc334e5e',
    'hex'
);

const org_keyPair = pcl.util.makeKeyPair();
org_keyPair.pubKey = orgPUB;
org_keyPair.privKey = orgPRIV;

const authDescriptor = new SingleSignatureAuthDescriptor(org_keyPair.pubKey, [FlagsType.Account, FlagsType.Transfer]);
export const organization = new User(org_keyPair, authDescriptor);