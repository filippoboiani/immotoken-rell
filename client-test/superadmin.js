import pcl from 'postchain-client';

import {
    SingleSignatureAuthDescriptor,
    FlagsType,
    User
  } from 'ft3-lib';

const superPRIV = Buffer.from(
    'b2798eb1ee84c81afd80aab29dbd8268cd4cd5a5e1f21e58c0b12ef65c7282bc',
    'hex'
);

const superPUB = Buffer.from(
    '037eb30ce10a5dc9929bcd15374b3db0b94b9e6450b73850161f65b0844ba065eb',
    'hex'
);

const super_keyPair = pcl.util.makeKeyPair();
super_keyPair.pubKey = superPUB;
super_keyPair.privKey = superPRIV;

const authDescriptor = new SingleSignatureAuthDescriptor(super_keyPair.pubKey,[FlagsType.Account, FlagsType.Transfer]);
export const superAdmin = new User(super_keyPair, authDescriptor);
