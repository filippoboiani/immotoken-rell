import {admin} from './admin.js'; // Use these to act as root!
import {organization} from './org.js'; // Use these to act as org!
import {manager} from './manager.js'; // Use these to act as manager/admin!
import {editor} from './editor.js'; // Use these to act as editor/admin!
import {investor} from './cli.js'; // Use these to act as investor!
import {AssetTokenizationSDK} from '../lib/src/main.js';



// Set session with init and admin keys. This enables you to run the root-commands
// with them you can add a add a fund. This can be seen as one whitlable-platform.
// You have to add one manager and one investor. the rest can be done by the manager.

// *** UNCOMMENT ***

const sdk = new AssetTokenizationSDK();
await sdk.init(admin.keyPair, admin.authDescriptor);
console.log("add organization...----------------------------")
try{await sdk.root_add_fund("bloqhouse", organization.authDescriptor);}catch(err){console.log(err)}
console.log("add manager...------------------------------")
await sdk.root_add_admin(manager.authDescriptor, true, "bloqhouse");
console.log("add investor...------------------------------")
await sdk.root_add_investor(investor.authDescriptor, true);
console.log("get manager...------------------------------")
const man = await sdk.getUserAccount(investor.authDescriptor.id);


// "Login as the manager"
// Create a property-object and push it on chain. 
// by default asset is set to not operable, meaning you have to activate it to be able to trade it.
// All asset-tokens will be held by the fund. Managers and editors can transfer on funds behalf
//
// We make the first transfer on the funds behalf from manager to investor 
// UTC TIME (will fix this)

// *** UNCOMMENT ***

console.log("new app...------------------------------")
const sdk2 = new AssetTokenizationSDK();
console.log("manager session...------------------------------")
await sdk2.init(manager.keyPair, manager.authDescriptor);

const start = Date.now();
const property = {
    name: "Real Estate 1",
    chainId: "42116EC4279E3BABD69FCE755DF8BB6C60797ECCEC193A280323B5AC7A796C11",
    nrOfFractions: 2000,
    initValue: 100000000,
    currency: 'EUR',
    start: start,
    end: start+90000000,
    extra: '{"name":"John", "age":30, "car":null}',
    manAuth: manager.authDescriptor.id
};

console.log("add prop...------------------------------")
await sdk2.addProperty(property);
console.log("get prop...------------------------------")
const asset = await sdk2.getAsset("Real Estate 1");
console.log("change prop operbable...------------------------------")
await sdk2.changeAssetOperable(asset.id, manager.authDescriptor.id, true);
console.log("transfer prop...------------------------------")
await sdk2.transfer_to_investor(manager.authDescriptor.id, asset.id, investor.authDescriptor.id, 20)
const options = { operable: true, type: 'Editor' }
console.log("add Editor...------------------------------")
await sdk2.addOrgUser(manager.authDescriptor.id, editor.authDescriptor, options);


// Act as editor and transfer asset

// *** UNCOMMENT ***

// console.log("new app...------------------------------")
// const sdk3 = new AssetTokenizationSDK();
// console.log("editor session...------------------------------")
// await sdk3.init(editor.keyPair, editor.authDescriptor);
// const asset = await sdk3.getAsset("Real Estate 1");
// console.log("get editor...------------------------------")
// await sdk3.getUserAccount(editor.authDescriptor.id);
// console.log("transfer from Editor...------------------------------")
// // this will fail if you run all code because the transfer is from the "same account" but with different auth_desc (manager/editor). Hence the chain sees it as reentrancy from same user 
// await sdk3.transfer_to_investor(editor.authDescriptor.id, asset.id, investor.authDescriptor.id, 30)
// await sdk3.getAssetHolders(asset.id);
// await sdk3.getAsset("Real Estate 1");

