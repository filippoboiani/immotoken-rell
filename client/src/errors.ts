export class MissingSessionError extends Error {
  name: 'MissingSessionError';
  constructor(public field: string, msg?: string) {
    super(msg);
  }
}

export function throwMissingSessionError () {
  throw new MissingSessionError(`Missing user session. Did you call createUserSession() before?`);
}
