import pcl from 'postchain-client';
import { environment as env } from '../environment/environment';


// useless... there is the `ConnectionClient`
// https://bitbucket.org/chromawallet/ft3-lib/src/master/client/lib/ft3/core/connection-client.ts
export class PostchainConnection {

  static create () {
    // TODO: understand what the '5' is...
    const rest = pcl.restClient.createRestClient(env.connectionUrl, env.blockchainRID, 5);
    const gtx = pcl.gtxClient.createClient(rest, Buffer.from(env.blockchainRID, 'hex'), []);
    return gtx;
  }
}
