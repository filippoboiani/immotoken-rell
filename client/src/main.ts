import { Postchain, Blockchain, SingleSignatureAuthDescriptor, FlagsType,
  User, KeyPair, MultiSignatureAuthDescriptor, BlockchainSession, op, nop, ConnectionClient } from 'ft3-lib';

import { environment as env } from '../environment/environment';
import { throwMissingSessionError } from './errors';
import { Property } from './models/property.model';

// TODO: move it to types.ts or create a model.ts file in case the AuthDescriptor gets used in other classes
declare type AuthDescriptor = SingleSignatureAuthDescriptor | MultiSignatureAuthDescriptor;

export class AssetTokenizationSDK {

  private _blockchain: Blockchain;
  private _session: BlockchainSession;
  private _connection: ConnectionClient;
  private _user: User;

  constructor (options?: any) {
    if (options && options.blockchainRID) {
      env.blockchainRID = options.blockchainRID;
    }
    if (options && options.blockchainURI) {
      env.connectionUrl = options.blockchainURI;
    }
  }

  public async init (keyPair?: KeyPair, authDescriptor?: AuthDescriptor) {
    const chainId = Buffer.from(env.blockchainRID, 'hex');
    this._blockchain = await new Postchain(env.connectionUrl).blockchain(chainId);
    this._connection = new ConnectionClient(env.connectionUrl, env.blockchainRID);
    if (keyPair) {
      this.createUserSession(keyPair, authDescriptor);
    }
  }

  /**
   * @function createUserSession
   * @description Create a blockchain session for the user identified by the private key.
   * The session is needed to interact with the blockchain code.
   *
   * @param {KeyPair | string} keyPair You can either pass the private key or the keypair.
   * @optional @param {AuthDescriptor} authDescriptor
   */
  public createUserSession (keyPair: KeyPair | string, authDescriptor?: AuthDescriptor): BlockchainSession {
    if (typeof keyPair === 'string') {
      keyPair = new KeyPair(keyPair);
    }
    if (!authDescriptor) {
      const authFlags = [FlagsType.Account, FlagsType.Transfer]; // RELAAAAAX this could be dangerous
      authDescriptor = new SingleSignatureAuthDescriptor(keyPair.pubKey, authFlags);
    }
    this._user = new User(keyPair, authDescriptor);
    this._session = this._blockchain.newSession(this._user);
    return this._session;
  }

  public root = {
    addFund: this.root_add_fund,
    addAdmin: this.root_add_admin,
    addInvestore: this.root_add_investor,
    changeUserOperable: this.root_operable_user
  }

  private async root_add_fund(_name: string, _user_auth: AuthDescriptor) {
    if (!this._session) { throwMissingSessionError(); }
    try {
      await this._session.call(op(`${env.rellModuleName}.root_add_organization`, _name, _user_auth));
    } catch (err) {
        throw err;
    }
  }

  private async root_add_admin(_user_auth: AuthDescriptor, _operable: boolean, _org: string) {
    if (!this._session) { throwMissingSessionError(); }
    try {
      await this._session.call(op(`${env.rellModuleName}.root_add_admin`, _user_auth, _operable, _org));
    } catch (err) {
      throw err;
    }
  }

  private async root_operable_user(_manAcc: string, _operable: boolean) {
    if (!this._session) { throwMissingSessionError(); }
    try {
      await this._session.call(op(`${env.rellModuleName}.root_operable_user`, _manAcc, _operable));
    } catch (err) {
      throw err;
    }
  }

  private async root_add_investor(_authDesc: AuthDescriptor, _operable: boolean) {
    if (!this._session) { throwMissingSessionError(); }
    try {
      await this._session.call(op(`${env.rellModuleName}.root_add_investor`, _authDesc, _operable));
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function addOrgUser
   * @description Add a user to an organization
   *
   * @param {Buffer} adminAccountId
   * @param {AuthDescriptor} userAuthDesc
   * @param {any} options { operable: true, type: 'Editor' }
   */
  public async addOrgUser (adminAuthId: Buffer, userAuthDesc: AuthDescriptor, options: any) {
    try {
      await this._session.call(
        op(`${env.rellModuleName}.add_org_representative`,
          adminAuthId,
          userAuthDesc,
          options.operable,
          options.type)
        );
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function addProperty
   * @description add a property. You must be a manager to run this operation.
   *
   * @param {Property} property
   */
  public async addProperty (property: Property) {
    if (!this._session) { throwMissingSessionError(); }

    try {
      await this._session.call(
        op(
          `${env.rellModuleName}.add_financial_instrument`,
          property.name,
          property.chainId,
          property.nrOfFractions,
          property.manAuth,
          property.initValue,
          property.currency,
          property.start,
          property.end,
          property.extra)
        );
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function changeAssetOperable
   * @description freeze or un-freeze an asset. A freezed asset can not be traded by anyone. Has to be manager to freeze/un-freeze
   * 
   * @param {Buffer} assetId 
   * @param {Buffer} userAuthId 
   * @param {Boolean} operable 
   */
   public async changeAssetOperable (assetId: Buffer, userAuthId: Buffer, operable: Boolean) {
    try {
      await this._session.call(
        op(`${env.rellModuleName}.operable_asset`,
          assetId,
          userAuthId,
          operable)
        );
    } catch (err) {
      throw err;
    }
  }

  
  /**
   * @function transfer_to_investor
   * @description The intial transfer from the fund to an investor. 
   * Investor, Asset and Manager/Editor has to be operable. The balance will be deducted from fund to investors wallet,
   * will not work if fund do not have the tokens. 
   * 
   * @param {Buffer} _manAuthId 
   * @param {Buffer} _assetId 
   * @param {Buffer} _investor 
   * @param {number} _nrOfShares 
   */
  public async transfer_to_investor(
    _manAuthId: Buffer, _assetId: Buffer,
    _investor: Buffer, _nrOfShares: number) {
    if (!this._session) { throwMissingSessionError(); }

    try {
      await this._session.call(
        op(
          `${env.rellModuleName}.initial_transfer_instrument`,
          _manAuthId,
          _assetId,
          _investor,
          _nrOfShares,
          [])
        );
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function getUserAccount
   * @description get the user account data by id.
   *
   * @param {string | Buffer} accountId
   */
   public async getUserAccount (accountId: string | Buffer) {
    if (accountId instanceof Buffer) {
      accountId = accountId.toString('hex');
    }
    try {
      return await this._connection.query(`${env.rellModuleName}.get_user_by_id`, { _id: accountId });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function getAsset
   * @description get the Asset data by name or id.
   * 
   * @param {string | Buffer} asset 
   */
  public async getAsset (asset: string | Buffer) {
    try {
      if (typeof asset === 'string'){
        return await this._connection.query(`${env.rellModuleName}.get_instrument_by_name`, { _name: asset });
      }else{
        return await this._connection.query(`${env.rellModuleName}.get_instrument_by_id`, { _id: asset });
      }
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function getAssetHolders
   * @description get all users that holds some amount of an asset and thier balance.
   * 
   * @param {Buffer} assetId 
   */
  public async getAssetHolders (assetId: Buffer) {
    try {
      return await this._connection.query(`${env.rellModuleName}.get_all_owners_of_asset`, { _asset_id: assetId });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @function getUserAssets
   * @description get user assets and balance of those assets
   * 
   * @param {Buffer} accountId 
   */
  public async getUserAssets (accountId: Buffer) {
    try {
      return await this._connection.query(`${env.rellModuleName}.get_all_assets_of_client`, { _client_id: accountId });
    } catch (err) {
      throw err;
    }
  }
}
