#!/bin/sh

NAME=$1

NG_BUILD_ARGS="prod"
if [ "$2" = "staging" ]
then
    NG_BUILD_ARGS="staging"
fi

###########################################
# make a node container to build everything

docker build -t ${NAME} -f -  . <<EOF
FROM node:10.18 as build-deps
WORKDIR /usr/src/app
COPY . ./
RUN npm install
RUN npm run-script build:${NG_BUILD_ARGS}
EOF
