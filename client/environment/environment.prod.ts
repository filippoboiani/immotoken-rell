export const environment = {
  appVersion: require('../package.json').version,
  // using default postchain node REST API port
    // connectionUrl: 'http://localhost:7743',
    connectionUrl: 'http://api.nilsfohlin.com',
  vaultUrl: 'https://dev.vault.chromia-development.com',
  explorerUrl: 'https://explorer-testnet.chromia.com/',
  // default blockchain identifier used for testing
  blockchainRID: '475BE3097749C8043D3B1910AC2C220C450F364A1E720F3AB6668F4F5CAE27C0',
  rellModuleName: 'bloqhouse_real_estate'
};
