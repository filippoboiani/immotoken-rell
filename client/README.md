# Client 

Get inspired by: https://bitbucket.org/chromawallet/ft3-lib/src/master/

## Import 
```
"dependencies": {
  "@chromia/real-estate-tokens": "git+ssh:// git@bitbucket.org:filippoboiani/immotoken-rell.git#v1.0.0-pre.0"
}

```
## Development

1. Build the library locally

```
npm run build:staging
OR 
npm run build:prod
```


2. Deploy the library

```
npm version <VERSION>
git push origin v3.0 <VERSION>
```
