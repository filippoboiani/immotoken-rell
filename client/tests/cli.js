import { blockchainRID, blockchainUrl } from './config.js'; // Blockchain config
import pcl from 'postchain-client';

import {
    Postchain,
    SingleSignatureAuthDescriptor,
    FlagsType,
    User
  } from 'ft3-lib';

const cliPRIV = Buffer.from(
    '544113b541f342a268c369a96f54d7e08388199fcc97639753a8cf73f58c39cb',
    'hex'
);

const cliPUB = Buffer.from(
    '0335cdc891678ba6e91c82b82f5b5e3ab97adcf9e2fb8a3ff4d1c9afb72d9e19db',
    'hex'
);

const client_keyPair = pcl.util.makeKeyPair();
client_keyPair.pubKey = cliPUB;
client_keyPair.privKey = cliPRIV;

const authDescriptor = new SingleSignatureAuthDescriptor(client_keyPair.pubKey,[FlagsType.Account, FlagsType.Transfer]);
export const client = new User(client_keyPair, authDescriptor);

const chainId = Buffer.from(blockchainRID, 'hex');
const blockchain = await new Postchain(blockchainUrl).blockchain(chainId);
export const clientSession = blockchain.newSession(client);





