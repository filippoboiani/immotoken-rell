//import { blockchainRID, blockchainUrl } from './config.js'; // Blockchain config
//import { admin, adminSession } from './admin.js'; // Use these to act as root!
// import { manager, managerSession } from './manager.js'; // Use these to act as manager!
// import { client, clientSession } from './cli.js'; // use these to act  as client!
// import pcl from 'postchain-client';

// import { op, Postchain, AssetBalance } from 'ft3-lib';

// const chainId = Buffer.from(blockchainRID, 'hex');
// const blockchain = await new Postchain(blockchainUrl).blockchain(chainId);

// const rest = pcl.restClient.createRestClient(blockchainUrl, blockchainRID, 5)
// const gtx = pcl.gtxClient.createClient(rest,Buffer.from(blockchainRID,'hex'),[]);

const sdk = new App();
sdk.init();
sdk.createUserSession(admin.keyPair);

// /**
//  * has to be admin to do this, thus the adminSession
//  *
//  * @param {string} _name
//  * @param {authDescriptor} _authDesc the managers authDescriptor
//  * @param {byte_array} _pubKey the managers pubkey
//  * @param {boolean} _operable is the manager operable instantly
//  */
// async function add_manager(_name, _authDesc, _pubKey, _operable){
//     try{
//         await adminSession.call(op('bloqhouse_real_estate.add_admin', _name, _authDesc, _pubKey, _operable));
//     }catch(e){
//         console.error(e);
//     }
// }

// /**
//  * has to be admin to do this, thus the adminSession
//  *
//  * @param {string} _name
//  * @param {authDescriptor} _authDesc the clients authDescriptor
//  * @param {byte_array} _pubKey the client pubkey
//  * @param {boolean} _operable is the clients operable instantly
//  */
// async function add_investor(_name, _authDesc, _pubKey, _operable){
//     try{
//         await adminSession.call(op('bloqhouse_real_estate.add_client', _name, _authDesc, _pubKey, _operable));
//     }catch(e){
//         console.error(e);
//     }
// }

// /**
//  * Set if manager is active == true or inactive == false
//  *
//  * @param {byte_array} _manAcc the managers account id
//  * @param {boolean} _boolean approve or disable a manager
//  */
//  async function change_manager_operable(_manAcc, _boolean){
//     try{
//         await adminSession.call(op('bloqhouse_real_estate.operable_admin', _manAcc, _boolean));
//     }catch(e){
//         console.error(e);
//     }
// }

// /**
//  * manager account has a name but should be changed to some sort of id etc but for dev purposes
//  *
//  * @param {string} _name
//  * @returns manager_account {account_id, username, operable}
//  */
//  async function get_manager_account(_name){
//     try{
//         const man = await gtx.query('bloqhouse_real_estate.get_admin_account', {name: _name});
//         return man;
//     }catch(e){
//         console.error(e);
//     }
// }

// async function get_investor_account(_name){
//     try{
//         const man = await gtx.query('bloqhouse_real_estate.get_client_account', {name: _name});
//         return man;
//     }catch(e){
//         console.error(e);
//     }
// }

// /**
//  * Has to be manager to do this - how to do this?!?!
//  *
//  * @param {string} _name
//  * @param {byte_array} _chainId
//  * @param {integer} _nrOfFractions
//  * @param {byte_array} _manAcc
//  */
// async function add_property(_name, _chainId, _nrOfFractions, _manAcc){
//     try{
//         await managerSession.call(op('bloqhouse_real_estate.add_financial_instrument', _name, _chainId, _nrOfFractions, _manAcc));
//     }catch(e){
//         console.error(e);
//     }
// }

// /**
//  * asset will have name, or fund-name which could be used as key
//  *
//  * @param {string} _name
//  * @returns asset {creator_manager, asset_name tokenId, tokenName, nrOfTokens}
//  */
// async function get_property_by_name(_name){
//     try{
//         const asset = await gtx.query('bloqhouse_real_estate.get_instrument_by_name', {name: _name});
//         return asset;
//     }catch(e){
//         console.error(e);
//     }
// }


// /**
//  *  Transfer asset shares to a client. Only the manager that is operable, and has created the property can transfer
//  *  the client has to be operable and since the transfer operation uses the ft3 module transfer, no check on nrofshars etc has to be done
//  * @param {Buffer} _manAccId
//  * @param {Buffer} _manAuthId
//  * @param {Buffer} _assetId
//  * @param {Buffer} _investor
//  * @param {integer} _nrOfShares
//  */
// async function transfer_to_investor(_manAccId, _manAuthId, _assetId, _investor, _nrOfShares){
//     try{
//         await managerSession.call(op('bloqhouse_real_estate.initial_transfer_instrument', _manAccId, _manAuthId, _assetId, _investor, _nrOfShares, []));
//     }catch(e){
//         console.error(e);
//     }
// }



// // TEST add manager, add investor, add property, transfer to investor

// const managerName = "Nils the King";
// const clientName = "Filipo the Queen"; // hahah
// const assetName = "Gothenburg Castle bich";
// const numberOfShares = 10000;

// console.log('***** Adding manager...');
// await add_manager(managerName, manager.authDescriptor, manager.keyPair.pubKey, false); // create manager
// console.log('***** Adding investor...');
// await add_investor(clientName, client.authDescriptor, client.keyPair.pubKey, true);
// console.log('***** Get investor account...');
// const clientAccount = await get_investor_account(clientName);
// console.log('***** Get manager account...');
// const managerAccount = await get_manager_account(managerName); // get the manager account
// console.log('***** Change manager account...');
// await change_manager_operable(managerAccount.account_id, true); // set manager to operble
// console.log('***** Add property...');
// await add_property(assetName, chainId, numberOfShares, managerAccount.account_id); // add asset with manager account
// console.log('***** Get property by name...');
// const asset = await get_property_by_name(assetName); // get the asset by name

// console.log("----------------------------------------------1----------------------------------------------")

// await transfer_to_investor(managerAccount.account_id, manager.authDescriptor, asset.tokenId, clientAccount.account_id, 10 );

// console.log("----------------------------------------------2----------------------------------------------")

// await AssetBalance.getByAccountAndAssetId(managerAccount.account_id, asset.tokenId, blockchain);

// console.log("----------------------------------------------2----------------------------------------------")

// await AssetBalance.getByAccountAndAssetId(clientAccount.account_id, asset.tokenId, blockchain);
