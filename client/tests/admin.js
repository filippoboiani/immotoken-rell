import { blockchainRID, blockchainUrl } from './config.js'; // these configs are set in previous section
import pcl from 'postchain-client';

import {
  Postchain,
  SingleSignatureAuthDescriptor,
  FlagsType,
  User
} from 'ft3-lib';

const adminPRIV = Buffer.from(
    '0101010101010101010101010101010101010101010101010101010101010101',
    'hex'
);

const adminPUB = Buffer.from(
    '031b84c5567b126440995d3ed5aaba0565d71e1834604819ff9c17f5e9d5dd078f',
    'hex'
);

const admin_keyPair = pcl.util.makeKeyPair();
admin_keyPair.pubKey = adminPUB;
admin_keyPair.privKey = adminPRIV;

const authDescriptor = new SingleSignatureAuthDescriptor(admin_keyPair.pubKey,[FlagsType.Account, FlagsType.Transfer]);
export const admin = new User(admin_keyPair, authDescriptor);

const chainId = Buffer.from(blockchainRID, 'hex');
const blockchain = await new Postchain(blockchainUrl).blockchain(chainId);
export const adminSession = blockchain.newSession(admin);
