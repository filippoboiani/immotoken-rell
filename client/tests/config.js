export const blockchainRID = "C4A81FEE225C5EE7FE0704B0A4A7085C0B55A2D8FBAF64920BCE7035235FD3EC";
//export const blockchainUrl = "http://api.nilsfohlin.com/"; // for fun on my node at home, also good for demo purpose
export const blockchainUrl = "http://localhost:7743"; // This is default value in node-config.properties file
export const vaultUrl = "https://dev.vault.chromia-development.com"; // Vault's url for SSO
