var path = require('path');

var libName = 'ChromiaRealEstate';
var outputFile = libName.toLocaleLowerCase() + '.min.js';

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

// for more info see: https://webpack.js.org/guides/author-libraries/#expose-the-library
module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: ['./src/index'],
  output: {
    path: path.resolve(__dirname, '../lib'),
    filename: outputFile,
    library: {
			name: libName,
			type: 'umd',
			umdNamedDefine: true
		},
    globalObject: 'this'
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['lib']
    }),
  ],
  optimization: {
		minimize: true,
    minimizer: [new TerserPlugin()]
	},
  resolve: {
     extensions: ['.ts', '.js', '.json'],
		 alias: {
			[path.resolve(__dirname, "environment/environment.ts")]:
				path.resolve(__dirname, "environment/environment.staging.ts")
	}
  },
  module: {
    rules: [{
      test: /\.(ts|js)x?$/,
      exclude: /(node_modules|bower_components|old)/,
      use: [
        // { loader: 'babel-loader' },
        { loader: 'ts-loader' },
      ]
    }]
  }
};
