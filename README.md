# Chromia Blokchain SDK - Documentation

## Getting Started

### Import the library

For now the way to import the library is the following:

1. Run the following:
```
npm install https://filippoboiani@bitbucket.org/filippoboiani/immotoken-rell.git#semver:v1.0.0-pre.1 --save
```

This will create the dependency: 
```
  "dependencies": {
    ...
    "@chromia/real-estate-tokens": "git+https://filippoboiani@bitbucket.org/filippoboiani/immotoken-rell.git#semver:v1.0.0-pre.1"
    ...
  }
```

`semver:v1.0.0-pre.1` will change every time we release a new version.

### Init the library

```
import { AssetTokenizationSDK } from '@chromia/asset-tokenization-sdk';

// 
const blockchainRID = '...';
const blockchainURI '...';
const SDK = new AssetTokenizationSDK({ blockchainRID, blockchainURI });

// initialize the SDK (will connect to the BC)
SDK.init();

const managerPrivateKey =
  '033eec59d704e88c7ea86a0a4dece8d455d712bcddb4b9f6e64b9fdec9f71f3ab8';
SDK.createUserSession(managerPrivateKey);
```

Notes: 
- The SDK stores the user session for a user so you can call as many operations as you want. 
- If you need to execute functions for different users, you must create different SDK instances.


## API reference

### `createUserSession`

Creates the session for the user identified by the private key. 

```
const managerPrivateKey =
  '033eec59d704e88c7ea86a0a4dece8d455d712bcddb4b9f6e64b9fdec9f71f3ab8';
SDK.createUserSession(managerPrivateKey);
```

### `getUserAccount`

Get a user account by account id.

```
const accountId = '...';
SDK.getUserAccount(accountId);
```

## Key handeling for dev
There are .js files with users and a user is just a set of keyPairs and an authdescriptor. The keyas are hard coded for easy dev so that all can use same "users" for test. 
However you might want to create more users, to do that we have to understand keypair and authdescriptors

Keypair is created like this:

### create keypair
```
import pcl from 'postchain-client';

const keyPair = pcl.util.makeKeyPair();

console.log("privKey: ", keyPair.privKey.toString('hex'), "pubKey: ", keyPair.pubKey.toString('hex'));
```

With a keypair you can create an authDescriptor. The authdescriptor essentially tells the chain what the keypair is allowed to do.

### create user
```
import pcl from 'postchain-client';

import {
    SingleSignatureAuthDescriptor,
    FlagsType,
    User
  } from 'ft3-lib';

const keyPair = pcl.util.makeKeyPair();

// authdescriptor with "A" and "T" flag.
const authDescriptor = new SingleSignatureAuthDescriptor(keyPair.pubKey,[FlagsType.Account, FlagsType.Transfer]);
const user = new User(keyPair, authDescriptor);
```

An example: 

The fund is actually a keypair or a "user". When the root creates it is added to the chain.

We do not want to operate the fund as the fund itself, we want to enable managers and editors to transfer asset on the funds behalf. To enable that we add the manager/editors authdescriptor to the fund user. 

1. create manager keypair
2. create manager authdescriptor with "A" and "T" flag, ("A" asset, "T" transfer)
3. add the manager as a user on chain AND add the managera authdescriptor to "fund-user" (this is done automatically with root_add_admin)

|KeyPair | Authdescriptor|
|------------- | -------------|
|fund.keyPair  | fund.authDescriptor|
|\-   | manager.authDescriptor ["A", "T"]|

Since the manager authDescriptor is created with "A" and "T" falg it can now act as the fund in all regards.

For Editor we might want to just add the "T" flag

1. create editor keypair
2. create editor authdescriptor with  "T" flag, ("T" transfer)
3. add the editor as a user on chain AND add the editor authdescriptor to "fund-user"

|KeyPair | Authdescriptor
|------------- | -------------|
|fund.keyPair  | fund.authDescriptor|
|\-   | manager.authDescriptor ["A", "T"]|
|\-   | editor.authDescriptor ["T"]|


So for example the only one who can add new admins or editors to a fund are the root or an Admin that is registerd to the fund with "A" flag. 