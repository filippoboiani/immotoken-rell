### Account Types
- Superuser (God-like)
- Admin
- Custodian: manages user keys and acts on behalf of the fund manager or the investor (until the user detaches from it)
- Fund Manager (or Financial Instrument Manager)
- Investor