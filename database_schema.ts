// adding missing firebase lib
declare namespace firebase {
  namespace firestore {
    export interface Timestamp {}
    export interface DocumentReference {}
  }

}
// adding missing type declarations...
declare type PrivateIdentification = any;
declare type BusinessIdentification = any;
declare type Pescheck = any;
declare type Idin = any;
declare type FormattedIdin = any;
declare type Investor = any;

// USER (INVESTOR)
/**
 * Defining the different roles for managers.
 */
export enum ManagerRole {
  Superadmin = 'superadmin',
  Admin = 'admin',
  Editor = 'editor',
}

// Workaround for the importing problem as JS object.
export const roles = Object.values(ManagerRole);
/**
 * This is the banhammer interface. Handles the complete disability
 * of the user to interact with the web application.
 */
export enum UserStatus {
  Disabled = 'disabled',
  Enabled = 'enabled',
}
/**
 * Representing our current KYC methods excluding Pescheck.
 */
export enum KYCMethods {
  Idin = 'idin',
  Private = 'private',
  Business = 'business',
}
/**
 * Defining the user's tier and what's his/her account privileges/data.
 */
export enum UserTier {
  Account = 'account',
  Investor = 'investor',
  Trader = 'trader',
}
/**
 * Main User interface. At this form, the user is on the 'account' tier since it does not have  advanced
 * to any other higher tier.
 */
export interface User {
  bankAccount?: string;
  createdDateTime: firebase.firestore.Timestamp;
  customId: number;
  email: string;
  id?: string;
  identificationRequest?: firebase.firestore.DocumentReference | PrivateIdentification | BusinessIdentification;
  idin?: firebase.firestore.DocumentReference | Idin;
  pescheck?: firebase.firestore.DocumentReference | Pescheck;
  status: UserStatus;
  statusMessage?: string;
  tier: UserTier;
  updatedDateTime: firebase.firestore.Timestamp;
}
/**
 * Basic User plus idin data transformed into legible properties.
 */
export interface IdinInvestor extends Omit <User, 'identificationRequest'>, FormattedIdin {
  kycMethod: KYCMethods.Idin;
  tier: UserTier.Investor;
  idin: firebase.firestore.DocumentReference | Idin;
}
/**
 * Basic User plus the private identification data.
 */
export interface PrivateInvestor extends Omit <User, 'idin' | 'bankAccount'> , Omit <PrivateIdentification, 'status'> {
  kycMethod: KYCMethods.Private;
  tier: UserTier.Investor;
  identificationRequest?: firebase.firestore.DocumentReference | PrivateIdentification;
}
/**
 * Basic User plus the business identification data.
 */
export interface BusinessInvestor
  extends Omit<User, 'idin' | 'bankAccount'> , Omit<BusinessIdentification, 'status'> {
  kycMethod: KYCMethods.Business;
  tier: UserTier.Investor;
  identificationRequest?: firebase.firestore.DocumentReference | BusinessIdentification;
}
/**
 * Data for a bank account change request.
 */
export interface BankAccountChange {
  id?: string;
  bankAccount: string;
  changed: boolean;
  previousBankAccount: string;
  userId: string;
  createdDateTime: firebase.firestore.Timestamp;
}
/**
 * Data for a bank account change request.
 */
export interface DataChangeRequest {
  type: 'name' | 'bankAccount';
  status: 'pending' | 'approved' | 'rejected';
  previousData: {
    name: string;
    surname: string;
  } | {bankAccount: string; };
  newData: {
    name: string;
    surname: string;
  } | { bankAccount: string; };
}
//////////////////////////////////////////////////////
// ASSET
export interface Asset {
  id?: string;
  brochure?: string[];
  city: string;
  country: string;
  createdDateTime: firebase.firestore.Timestamp;
  deleted: boolean;
  dividendsFormat: {
    contents: [string, number]
  } [];
  emissionCost: number;
  euroMin: number;
  fixedDividends?: boolean;
  floorPlanImages?: string[];
  houseNumber: string;
  images?: string[];
  investmentCase: string;
  name: string;
  postalCode: string;
  premium: boolean;
  propertyDetails: string;
  prospectus?: string[];
  published: boolean;
  returnsAfterEnd?: number;
  sharePrice: number;
  sharesAvailable: number;
  startDateTime: firebase.firestore.Timestamp;
  endDateTime: firebase.firestore.Timestamp;
  street: string;
  totalValueEuro: number;
  totalValueShares: number;
  updatedDateTime?: firebase.firestore.Timestamp;
  addresses?: AssetAddress[];
}
export interface AssetAddress {
  country: string;
  city: string;
  street: string;
  houseNumber: string;
}

////////////////////////////////////////////////////// // INVESTMENT
/**
 * The upper db object for an Investment.
 * An investments always contains a subcollection of Payments. */
export interface Investment {
  id?: string;
  investor: firebase.firestore.DocumentReference | Investor;
  asset: firebase.firestore.DocumentReference | Asset;
  paidEuroTotal?: number;
  boughtSharesTotal?: number;
  createdDateTime: firebase.firestore.Timestamp;
  updatedDateTime?: firebase.firestore.Timestamp;
}
/**
 * Defining all the statuses a Payment can have.
 */
export const enum PaymentStatus {
  Open = 'open',
  Canceled = 'canceled',
  Pending = 'pending',
  Authorized = 'authorized',
  Expired = 'expired',
  Failed = 'failed',
  Paid = 'paid',
}

/**
 * Different payment gateways/providers we use.
 */
export enum PaymentProvider {
  Mollie = 'Mollie',
  Custom = 'Custom', // (Manual, from Bloqadmin.)
}

/**
 * Actual payment info is placed here.
 */
export interface Payment {
  id?: string;
  investor: firebase.firestore.DocumentReference | Investor;
  investment: firebase.firestore.DocumentReference | Investment;
  asset: firebase.firestore.DocumentReference | Asset;
  dividendsFormat: [string, number];
  provider: PaymentProvider;
  providerData: {
    id: string,
    amount: {
      currency: string,
      value: string | number;
    };
    status: PaymentStatus,
    metadata: {
      uid?: string,
      euroAmount: number,
      sharesAmount: number,
      investmentId?: string,
      assetId?: string,
      paymentId?: string,
      selectedDividendsFormatYear?: [string, number];
    }
    [key: string]: any;
  };
  deleted: boolean;
  ended?: firebase.firestore.Timestamp;
  createdDateTime: firebase.firestore.Timestamp;
  updatedDateTime?: firebase.firestore.Timestamp;
  paymentDateTime?: firebase.firestore.Timestamp;
}
