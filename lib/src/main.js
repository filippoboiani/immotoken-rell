"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssetTokenizationSDK = void 0;
var ft3_lib_1 = require("ft3-lib");
var environment_1 = require("../environment/environment");
var errors_1 = require("./errors");
var AssetTokenizationSDK = /** @class */ (function () {
    function AssetTokenizationSDK(options) {
        this.root = {
            addFund: this.root_add_fund,
            addAdmin: this.root_add_admin,
            addInvestore: this.root_add_investor,
            changeUserOperable: this.root_operable_user
        };
        if (options && options.blockchainRID) {
            environment_1.environment.blockchainRID = options.blockchainRID;
        }
        if (options && options.blockchainURI) {
            environment_1.environment.connectionUrl = options.blockchainURI;
        }
    }
    AssetTokenizationSDK.prototype.init = function (keyPair, authDescriptor) {
        return __awaiter(this, void 0, void 0, function () {
            var chainId, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        chainId = Buffer.from(environment_1.environment.blockchainRID, 'hex');
                        _a = this;
                        return [4 /*yield*/, new ft3_lib_1.Postchain(environment_1.environment.connectionUrl).blockchain(chainId)];
                    case 1:
                        _a._blockchain = _b.sent();
                        this._connection = new ft3_lib_1.ConnectionClient(environment_1.environment.connectionUrl, environment_1.environment.blockchainRID);
                        if (keyPair) {
                            this.createUserSession(keyPair, authDescriptor);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function createUserSession
     * @description Create a blockchain session for the user identified by the private key.
     * The session is needed to interact with the blockchain code.
     *
     * @param {KeyPair | string} keyPair You can either pass the private key or the keypair.
     * @optional @param {AuthDescriptor} authDescriptor
     */
    AssetTokenizationSDK.prototype.createUserSession = function (keyPair, authDescriptor) {
        if (typeof keyPair === 'string') {
            keyPair = new ft3_lib_1.KeyPair(keyPair);
        }
        if (!authDescriptor) {
            var authFlags = [ft3_lib_1.FlagsType.Account, ft3_lib_1.FlagsType.Transfer]; // RELAAAAAX this could be dangerous
            authDescriptor = new ft3_lib_1.SingleSignatureAuthDescriptor(keyPair.pubKey, authFlags);
        }
        this._user = new ft3_lib_1.User(keyPair, authDescriptor);
        this._session = this._blockchain.newSession(this._user);
        return this._session;
    };
    AssetTokenizationSDK.prototype.root_add_fund = function (_name, _user_auth) {
        return __awaiter(this, void 0, void 0, function () {
            var err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._session) {
                            errors_1.throwMissingSessionError();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".root_add_organization", _name, _user_auth))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        throw err_1;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AssetTokenizationSDK.prototype.root_add_admin = function (_user_auth, _operable, _org) {
        return __awaiter(this, void 0, void 0, function () {
            var err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._session) {
                            errors_1.throwMissingSessionError();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".root_add_admin", _user_auth, _operable, _org))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_2 = _a.sent();
                        throw err_2;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AssetTokenizationSDK.prototype.root_operable_user = function (_manAcc, _operable) {
        return __awaiter(this, void 0, void 0, function () {
            var err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._session) {
                            errors_1.throwMissingSessionError();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".root_operable_user", _manAcc, _operable))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_3 = _a.sent();
                        throw err_3;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AssetTokenizationSDK.prototype.root_add_investor = function (_authDesc, _operable) {
        return __awaiter(this, void 0, void 0, function () {
            var err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._session) {
                            errors_1.throwMissingSessionError();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".root_add_investor", _authDesc, _operable))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_4 = _a.sent();
                        throw err_4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function addOrgUser
     * @description Add a user to an organization
     *
     * @param {Buffer} adminAccountId
     * @param {AuthDescriptor} userAuthDesc
     * @param {any} options { operable: true, type: 'Editor' }
     */
    AssetTokenizationSDK.prototype.addOrgUser = function (adminAuthId, userAuthDesc, options) {
        return __awaiter(this, void 0, void 0, function () {
            var err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".add_org_representative", adminAuthId, userAuthDesc, options.operable, options.type))];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        throw err_5;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function addProperty
     * @description add a property. You must be a manager to run this operation.
     *
     * @param {Property} property
     */
    AssetTokenizationSDK.prototype.addProperty = function (property) {
        return __awaiter(this, void 0, void 0, function () {
            var err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._session) {
                            errors_1.throwMissingSessionError();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".add_financial_instrument", property.name, property.chainId, property.nrOfFractions, property.manAuth, property.initValue, property.currency, property.start, property.end, property.extra))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_6 = _a.sent();
                        throw err_6;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function changeAssetOperable
     * @description freeze or un-freeze an asset. A freezed asset can not be traded by anyone. Has to be manager to freeze/un-freeze
     *
     * @param {Buffer} assetId
     * @param {Buffer} userAuthId
     * @param {Boolean} operable
     */
    AssetTokenizationSDK.prototype.changeAssetOperable = function (assetId, userAuthId, operable) {
        return __awaiter(this, void 0, void 0, function () {
            var err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".operable_asset", assetId, userAuthId, operable))];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_7 = _a.sent();
                        throw err_7;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function transfer_to_investor
     * @description The intial transfer from the fund to an investor.
     * Investor, Asset and Manager/Editor has to be operable. The balance will be deducted from fund to investors wallet,
     * will not work if fund do not have the tokens.
     *
     * @param {Buffer} _manAuthId
     * @param {Buffer} _assetId
     * @param {Buffer} _investor
     * @param {number} _nrOfShares
     */
    AssetTokenizationSDK.prototype.transfer_to_investor = function (_manAuthId, _assetId, _investor, _nrOfShares) {
        return __awaiter(this, void 0, void 0, function () {
            var err_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._session) {
                            errors_1.throwMissingSessionError();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._session.call(ft3_lib_1.op(environment_1.environment.rellModuleName + ".initial_transfer_instrument", _manAuthId, _assetId, _investor, _nrOfShares, []))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_8 = _a.sent();
                        throw err_8;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function getUserAccount
     * @description get the user account data by id.
     *
     * @param {string | Buffer} accountId
     */
    AssetTokenizationSDK.prototype.getUserAccount = function (accountId) {
        return __awaiter(this, void 0, void 0, function () {
            var err_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (accountId instanceof Buffer) {
                            accountId = accountId.toString('hex');
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._connection.query(environment_1.environment.rellModuleName + ".get_user_by_id", { _id: accountId })];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        err_9 = _a.sent();
                        throw err_9;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function getAsset
     * @description get the Asset data by name or id.
     *
     * @param {string | Buffer} asset
     */
    AssetTokenizationSDK.prototype.getAsset = function (asset) {
        return __awaiter(this, void 0, void 0, function () {
            var err_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        if (!(typeof asset === 'string')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._connection.query(environment_1.environment.rellModuleName + ".get_instrument_by_name", { _name: asset })];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2: return [4 /*yield*/, this._connection.query(environment_1.environment.rellModuleName + ".get_instrument_by_id", { _id: asset })];
                    case 3: return [2 /*return*/, _a.sent()];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        err_10 = _a.sent();
                        throw err_10;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function getAssetHolders
     * @description get all users that holds some amount of an asset and thier balance.
     *
     * @param {Buffer} assetId
     */
    AssetTokenizationSDK.prototype.getAssetHolders = function (assetId) {
        return __awaiter(this, void 0, void 0, function () {
            var err_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._connection.query(environment_1.environment.rellModuleName + ".get_all_owners_of_asset", { _asset_id: assetId })];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2:
                        err_11 = _a.sent();
                        throw err_11;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @function getUserAssets
     * @description get user assets and balance of those assets
     *
     * @param {Buffer} accountId
     */
    AssetTokenizationSDK.prototype.getUserAssets = function (accountId) {
        return __awaiter(this, void 0, void 0, function () {
            var err_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._connection.query(environment_1.environment.rellModuleName + ".get_all_assets_of_client", { _client_id: accountId })];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2:
                        err_12 = _a.sent();
                        throw err_12;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return AssetTokenizationSDK;
}());
exports.AssetTokenizationSDK = AssetTokenizationSDK;
//# sourceMappingURL=main.js.map