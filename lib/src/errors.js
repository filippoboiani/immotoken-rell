"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.throwMissingSessionError = exports.MissingSessionError = void 0;
var MissingSessionError = /** @class */ (function (_super) {
    __extends(MissingSessionError, _super);
    function MissingSessionError(field, msg) {
        var _this = _super.call(this, msg) || this;
        _this.field = field;
        return _this;
    }
    return MissingSessionError;
}(Error));
exports.MissingSessionError = MissingSessionError;
function throwMissingSessionError() {
    throw new MissingSessionError("Missing user session. Did you call createUserSession() before?");
}
exports.throwMissingSessionError = throwMissingSessionError;
//# sourceMappingURL=errors.js.map