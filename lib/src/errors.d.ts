export declare class MissingSessionError extends Error {
    field: string;
    name: 'MissingSessionError';
    constructor(field: string, msg?: string);
}
export declare function throwMissingSessionError(): void;
