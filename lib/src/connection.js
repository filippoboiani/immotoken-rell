"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostchainConnection = void 0;
var postchain_client_1 = __importDefault(require("postchain-client"));
var environment_1 = require("../environment/environment");
// useless... there is the `ConnectionClient`
// https://bitbucket.org/chromawallet/ft3-lib/src/master/client/lib/ft3/core/connection-client.ts
var PostchainConnection = /** @class */ (function () {
    function PostchainConnection() {
    }
    PostchainConnection.create = function () {
        // TODO: understand what the '5' is...
        var rest = postchain_client_1.default.restClient.createRestClient(environment_1.environment.connectionUrl, environment_1.environment.blockchainRID, 5);
        var gtx = postchain_client_1.default.gtxClient.createClient(rest, Buffer.from(environment_1.environment.blockchainRID, 'hex'), []);
        return gtx;
    };
    return PostchainConnection;
}());
exports.PostchainConnection = PostchainConnection;
//# sourceMappingURL=connection.js.map