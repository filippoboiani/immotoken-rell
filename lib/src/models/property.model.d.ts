export interface Property {
    name: string;
    chainId: string;
    nrOfFractions: number;
    initValue: string;
    currency: string;
    start: number;
    end: number;
    extra: any;
    manAuth: any;
}
