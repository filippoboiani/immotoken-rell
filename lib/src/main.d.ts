import { SingleSignatureAuthDescriptor, KeyPair, MultiSignatureAuthDescriptor, BlockchainSession } from 'ft3-lib';
import { Property } from './models/property.model';
declare type AuthDescriptor = SingleSignatureAuthDescriptor | MultiSignatureAuthDescriptor;
export declare class AssetTokenizationSDK {
    private _blockchain;
    private _session;
    private _connection;
    private _user;
    constructor(options?: any);
    init(keyPair?: KeyPair, authDescriptor?: AuthDescriptor): Promise<void>;
    /**
     * @function createUserSession
     * @description Create a blockchain session for the user identified by the private key.
     * The session is needed to interact with the blockchain code.
     *
     * @param {KeyPair | string} keyPair You can either pass the private key or the keypair.
     * @optional @param {AuthDescriptor} authDescriptor
     */
    createUserSession(keyPair: KeyPair | string, authDescriptor?: AuthDescriptor): BlockchainSession;
    root: {
        addFund: (_name: string, _user_auth: AuthDescriptor) => Promise<void>;
        addAdmin: (_user_auth: AuthDescriptor, _operable: boolean, _org: string) => Promise<void>;
        addInvestore: (_authDesc: AuthDescriptor, _operable: boolean) => Promise<void>;
        changeUserOperable: (_manAcc: string, _operable: boolean) => Promise<void>;
    };
    private root_add_fund;
    private root_add_admin;
    private root_operable_user;
    private root_add_investor;
    /**
     * @function addOrgUser
     * @description Add a user to an organization
     *
     * @param {Buffer} adminAccountId
     * @param {AuthDescriptor} userAuthDesc
     * @param {any} options { operable: true, type: 'Editor' }
     */
    addOrgUser(adminAuthId: Buffer, userAuthDesc: AuthDescriptor, options: any): Promise<void>;
    /**
     * @function addProperty
     * @description add a property. You must be a manager to run this operation.
     *
     * @param {Property} property
     */
    addProperty(property: Property): Promise<void>;
    /**
     * @function changeAssetOperable
     * @description freeze or un-freeze an asset. A freezed asset can not be traded by anyone. Has to be manager to freeze/un-freeze
     *
     * @param {Buffer} assetId
     * @param {Buffer} userAuthId
     * @param {Boolean} operable
     */
    changeAssetOperable(assetId: Buffer, userAuthId: Buffer, operable: Boolean): Promise<void>;
    /**
     * @function transfer_to_investor
     * @description The intial transfer from the fund to an investor.
     * Investor, Asset and Manager/Editor has to be operable. The balance will be deducted from fund to investors wallet,
     * will not work if fund do not have the tokens.
     *
     * @param {Buffer} _manAuthId
     * @param {Buffer} _assetId
     * @param {Buffer} _investor
     * @param {number} _nrOfShares
     */
    transfer_to_investor(_manAuthId: Buffer, _assetId: Buffer, _investor: Buffer, _nrOfShares: number): Promise<void>;
    /**
     * @function getUserAccount
     * @description get the user account data by id.
     *
     * @param {string | Buffer} accountId
     */
    getUserAccount(accountId: string | Buffer): Promise<any>;
    /**
     * @function getAsset
     * @description get the Asset data by name or id.
     *
     * @param {string | Buffer} asset
     */
    getAsset(asset: string | Buffer): Promise<any>;
    /**
     * @function getAssetHolders
     * @description get all users that holds some amount of an asset and thier balance.
     *
     * @param {Buffer} assetId
     */
    getAssetHolders(assetId: Buffer): Promise<any>;
    /**
     * @function getUserAssets
     * @description get user assets and balance of those assets
     *
     * @param {Buffer} accountId
     */
    getUserAssets(accountId: Buffer): Promise<any>;
}
export {};
