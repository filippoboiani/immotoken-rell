export declare const environment: {
    connectionUrl: string;
    vaultUrl: string;
    explorerUrl: string;
    blockchainRID: string;
    rellModuleName: string;
};
